# -*- mode: python -*-

block_cipher = None

import os
cwd_str = os.getcwd()

##### ['mindfulness-at-the-computer.py']
a = Analysis(
    ['mc/main.py'],
    pathex=[cwd_str],
    binaries=[],
    datas=[('README.md', '.'), ('LICENSE.txt', '.'), ('matc.qss', '.')],
    hiddenimports=[],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher
)

# Adding the res and icons directories
# Please note that when adding individual files this is done above - inside "Analysis"
# res_dir_str = "res"
# a.datas += Tree('./' + res_dir_str, prefix=res_dir_str, excludes=['*.db'])
# -documentation: https://pythonhosted.org/PyInstaller/advanced-topics.html#the-tree-class

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(
    pyz,
    a.scripts,
    exclude_binaries=True,
    name='mindfulness-at-the-computer',
    debug=False,
    strip=False,
    upx=True,
    console=True
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    name='mindfulness-at-the-computer'
)
