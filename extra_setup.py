#!/usr/bin/env python3

import os.path
import shutil
from string import Template
from PyQt5 import QtCore
import mc.globa


def do_extra_setup():
    if QtCore.QSysInfo.kernelType() != "linux":
        print("Only gnu/linux systems can run this extra setup file at the moment")
        return
    print("====Running extra setup python script extra_setup.py====")
    user_home_dir = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.HomeLocation)[0]
    # print(f"{user_home_dir=}")
    user_config_dir = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.ConfigLocation)[0]
    # print(f"{user_config_dir=}")
    user_applications_dir = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.ApplicationsLocation)[0]
    # print(f"{user_applications_dir=}")  # -Ex: ~/.local/share/applications/

    autostart_dir = os.path.join(user_config_dir, "autostart")
    print(f"{autostart_dir=}")  # -Ex: ~/.config/autostart/

    appl_base_dir_str: str = os.path.dirname(os.path.abspath(__file__))
    print(f"{appl_base_dir_str=}")

    template_desktop_file_path: str = os.path.join("varia", "mindfulness-at-the-computer[template].desktop")
    output_desktop_file_name: str = "mindfulness-at-the-computer.desktop"

    with open(template_desktop_file_path, "r") as f:
        content_str = f.read()
        template = Template(content_str)
        # -https://doc.qt.io/qt-5/qstandardpaths.html#StandardLocation-enum
        exec_path: str = os.path.join(user_home_dir, ".local", "bin", mc.globa.APPLICATION_SCRIPT_NAME_STR)
        icon_path: str = os.path.join(appl_base_dir_str, "mc", "res/icons", "icon.png")
        output_desktop_file_contents: str = template.substitute(exec=exec_path, icon=icon_path)
        with open(output_desktop_file_name, "w+") as output_file:
            output_file.write(output_desktop_file_contents)
        print(f"Copying {output_desktop_file_name} to {user_applications_dir}")
        shutil.copy(output_desktop_file_name, user_applications_dir)
        print(f"Copying {output_desktop_file_name} to {autostart_dir}")
        shutil.copy(output_desktop_file_name, autostart_dir)
        print("====")


if __name__ == "__main__":
    do_extra_setup()

"""

### Freedesktop spec

https://www.freedesktop.org/wiki/Specifications/autostart-spec/
https://specifications.freedesktop.org/autostart-spec/autostart-spec-latest.html

### Autostart dir

$XDG_CONFIG_HOME defines the base directory relative to which user-specific configuration files should be stored.
If $XDG_CONFIG_HOME is either not set or empty, a default equal to $HOME/.config should be used.

Based on the info above this is the default location:
.desktop file in ~/.config/autostart

SZs computer: ~/.config/autostart

### User applications dir

SZs computer: ~/.local/share/applications

"""
