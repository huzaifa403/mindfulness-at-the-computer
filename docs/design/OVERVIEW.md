# Design Overview

Design documentation: See files in this directory

Links:

* [Gitter room: design-group](https://gitter.im/mindfulness-at-the-computer/design-group)
* [Feature ideas (wiki)](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/wikis/Feature-Ideas)
* [Open issues with the design tag](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=design)
* [Qt Style Sheets documentation](https://doc.qt.io/qt-5/stylesheet.html)

Process:

1. Discussing changes with the others [on Gitter](https://gitter.im/mindfulness-at-the-computer/community)
2. Creating a new issue with the design tag
  * Perhaps also the question tag if it is unsure if this is going to be implemented
3. Updating the design documentation
4. Implementing
