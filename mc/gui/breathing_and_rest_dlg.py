import logging
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5 import QtGui
import mc.globa
# import mc.model

WINDOW_FLAGS = (
    QtCore.Qt.Dialog
    | QtCore.Qt.FramelessWindowHint
    | QtCore.Qt.WindowStaysOnTopHint
    | QtCore.Qt.WindowDoesNotAcceptFocus
)
# QtCore.Qt.BypassWindowManagerHint


class BreathingAndRestDlg(QtWidgets.QDialog):
    close_signal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()

        self.setWindowFlags(WINDOW_FLAGS)
        self.setWindowTitle(mc.globa.APPLICATION_TITLE_STR)
        self.setWindowIcon(QtGui.QIcon(mc.globa.get_app_icon_path("icon.png")))
        self.setStyleSheet(
            f"background-color: {mc.globa.MC_BLACK_COLOR_STR};"
            "color: #999999;"
            f"selection-background-color: {mc.globa.MC_LIGHT_GREEN_COLOR_STR};"
            "selection-color:#000000;"
        )

        self.vbox_l2 = QtWidgets.QVBoxLayout()
        self.vbox_l2.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.vbox_l2)

        # Rest widget

        self.rest_widget_w3 = QtWidgets.QWidget()  # -this is added to the layout in the show_full_rest_dialog function
        vbox_l4 = QtWidgets.QVBoxLayout()
        self.rest_widget_w3.setLayout(vbox_l4)

        self.actions_list_vbox_l5 = QtWidgets.QVBoxLayout()
        vbox_l4.addLayout(self.actions_list_vbox_l5)
        self.setup_rest_action_list()
        """
        walking_mindfully_qll = QtWidgets.QLabel(self.tr("Please move and walk mindfully when leaving the computer"))
        walking_mindfully_qll.setFont(mc.globa.get_font_xxlarge())
        walking_mindfully_qll.setWordWrap(True)
        vbox_l4.addWidget(walking_mindfully_qll, stretch=3)
        """

        """
        self.close_qpb = QtWidgets.QPushButton(self.tr("Close"))
        size_policy = QtWidgets.QSizePolicy()
        size_policy.setHorizontalPolicy(QtWidgets.QSizePolicy.Minimum)
        self.close_qpb.setSizePolicy(size_policy)
        vbox_l4.addWidget(self.close_qpb, alignment=QtCore.Qt.AlignHCenter)
        font = self.close_qpb.font()
        font.setPointSize(20)
        self.close_qpb.setFont(font)
        self.close_qpb.setMinimumWidth(512)
        self.close_qpb.clicked.connect(self.on_close_clicked)
        """

        # Breathing dialog/widget

        self.breathing_dlg = BreathingGraphicsView()
        self.vbox_l2.addWidget(self.breathing_dlg, alignment=QtCore.Qt.AlignHCenter)
        self.breathing_dlg.rest_btn_clicked_signal.connect(self.on_brdlg_rest_btn_clicked)
        self.breathing_dlg.mouse_btn_released_signal.connect(self.on_brdlg_mouse_btn_released)
        self.breathing_dlg.mouse_leave_signal.connect(self.on_brdlg_mouse_leave)
        # self.breathing_dlg.setContentsMargins(0, 0, 0, 0)
        self.breathing_dlg.show()

    def close_dlg(self):
        # , i_closed_from_rest: bool

        # self.showNormal()
        # -for MacOS. showNormal is used here rather than showMinimized to avoid animation
        self.breathing_dlg.ib_qtimeline.stop()
        self.breathing_dlg.ob_qtimeline.stop()
        self.breathing_dlg.circle_go.setScale(1)
        self.breathing_dlg.breathing_count_int = 0
        self.breathing_dlg.hide()
        self.hide()
        self.close_signal.emit()

    def on_brdlg_mouse_btn_released(self):
        if not self.isFullScreen():
            self.close_dlg()

    def on_brdlg_mouse_leave(self):
        if not self.isFullScreen():
            self.close_dlg()

    def on_brdlg_rest_btn_clicked(self):
        if self.isFullScreen():
            self.show_breathing_only()
        else:
            self.show_full_rest_dialog()

    def show_full_rest_dialog(self):

        mc.globa.nr_of_breathing_reminders_since_last_rest = 0
        mc.globa.is_rest_reminder_shown = False

        mc.globa.breathing_state = mc.globa.BreathingState.inactive
        # self.breathing_dlg.breathing_gi.update_pos_and_origin_point(VIEW_WIDTH_INT, VIEW_HEIGHT_INT)
        self.showFullScreen()
        self.breathing_dlg.show()
        self.vbox_l2.insertWidget(0, self.rest_widget_w3)
        self.rest_widget_w3.show()

    def show_breathing_only(self):
        mc.globa.breathing_state = mc.globa.BreathingState.inactive
        # self.breathing_dlg.breathing_gi.update_pos_and_origin_point(VIEW_WIDTH_INT, VIEW_HEIGHT_INT)
        self.showNormal()
        self.breathing_dlg.show()
        self.vbox_l2.removeWidget(self.rest_widget_w3)
        self.rest_widget_w3.hide()
        self.adjustSize()  # -also seems to set x and y to 0
        screen_qrect = QtWidgets.QApplication.desktop().availableGeometry()
        _xpos_int = screen_qrect.left() + (screen_qrect.width() - VIEW_WIDTH_INT) // 2
        _ypos_int = screen_qrect.bottom() - VIEW_HEIGHT_INT + 1
        # -self.sizeHint().height() gives only 52 here, unknown why, so we use VIEW_HEIGHT_INT instead
        # logging.debug("screen_qrect.bottom() = " + str(screen_qrect.bottom()))
        # logging.debug("self.sizeHint().height() = " + str(self.sizeHint().height()))
        self.move(_xpos_int, _ypos_int)

        # self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        # self.breathing_dlg.setAttribute(QtCore.Qt.WA_TranslucentBackground)

    def on_close_clicked(self):
        self.close_dlg()

    def setup_rest_action_list(self):
        mc.globa.clear_widget_and_layout_children(self.actions_list_vbox_l5)
        rest_actions: list[mc.globa.RestAction] = mc.globa.settings.get(mc.globa.SK_REST_ACTIONS)
        self.actions_list_vbox_l5.addStretch(1)
        for ra in rest_actions:
            rest_action_title_qll = QtWidgets.QLabel(ra.title)
            # rest_action_title_qll.setWordWrap(True)
            rest_action_title_qll.setFont(mc.globa.get_font_xlarge())
            rest_action_title_qll.setContentsMargins(10, 5, 10, 5)
            self.actions_list_vbox_l5.addWidget(rest_action_title_qll, alignment=QtCore.Qt.AlignHCenter)
        self.actions_list_vbox_l5.addStretch(1)


TIME_NOT_SET_FT = 0.0

MIN_SCALE_FT = 0.7
HISTORY_IB_MAX = 4.0
HISTORY_OB_MAX = 7.0
TIME_LINE_IB_DURATION_INT = 8000
TIME_LINE_OB_DURATION_INT = 16000
TIME_LINE_IB_FRAME_RANGE_INT = 1000
TIME_LINE_OB_FRAME_RANGE_INT = 2000

VIEW_WIDTH_INT = 400
VIEW_HEIGHT_INT = 250
BR_WIDTH_FT = 90.0
BR_HEIGHT_FT = 90.0
REST_BTN_SIZE = 30


class BreathingGraphicsView(QtWidgets.QGraphicsView):
    mouse_btn_released_signal = QtCore.pyqtSignal()
    mouse_leave_signal = QtCore.pyqtSignal()
    rest_btn_clicked_signal = QtCore.pyqtSignal()
    first_breathing_gi_signal = QtCore.pyqtSignal()

    # Also contains the graphics scene
    def __init__(self, i_can_be_closed: bool = True) -> None:
        super().__init__()

        self.breathing_count_int = 0
        self._can_be_closed_bool = i_can_be_closed
        self._keyboard_active_bool = True
        self._start_time_ft = TIME_NOT_SET_FT
        self._ib_length_ft_list = []
        self._ob_length_ft_list = []
        self._peak_scale_ft = 1

        # Window setup
        self.setFrameStyle(QtWidgets.QFrame.Box | QtWidgets.QFrame.Plain)
        self.setLineWidth(0)
        # self.setStyleSheet("border-radius: 15px;")
        self.setFixedWidth(VIEW_WIDTH_INT)
        self.setFixedHeight(VIEW_HEIGHT_INT)
        t_brush = QtGui.QBrush(QtGui.QColor(mc.globa.MC_BLACK_COLOR_STR))
        self.setBackgroundBrush(t_brush)
        self.setRenderHints(
            QtGui.QPainter.Antialiasing |
            QtGui.QPainter.SmoothPixmapTransform
        )
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        # ..set position
        screen_qrect = QtWidgets.QApplication.desktop().availableGeometry()
        self._xpos_int = screen_qrect.left() + (screen_qrect.width() - VIEW_WIDTH_INT) // 2
        self._ypos_int = screen_qrect.bottom() - VIEW_HEIGHT_INT - 60
        # -self.sizeHint().height() gives only 52 here, unknown why, so we use VIEW_HEIGHT_INT instead
        self.move(self._xpos_int, self._ypos_int)

        # Graphics and layout setup..
        vbox_l2 = QtWidgets.QVBoxLayout()
        self.setLayout(vbox_l2)
        # ..graphics scene
        self._graphics_scene = QtWidgets.QGraphicsScene()
        self._graphics_scene.setSceneRect(QtCore.QRectF(0, 0, VIEW_WIDTH_INT, VIEW_HEIGHT_INT))
        self.setScene(self._graphics_scene)
        # ..rest button
        self._rest_button_gi = RestButtonGraphicsObject()
        self._rest_button_gi.clicked_signal.connect(self.on_rest_button_clicked)
        self._rest_button_gi.setPos(VIEW_WIDTH_INT - REST_BTN_SIZE, 0)
        self._graphics_scene.addItem(self._rest_button_gi)
        # ..custom dynamic breathing graphic (may be possible to change this in the future)
        self.circle_go = BreathingCircleGraphicsObject()
        self._graphics_scene.addItem(self.circle_go)
        self.circle_go.update_pos_and_origin_point(VIEW_WIDTH_INT, VIEW_HEIGHT_INT)
        self.circle_go.hover_signal.connect(self._breathing_gi_hover)
        # -Please note that for breathing in we use a static sized rectangle (instead of the one the user sees),
        #  this is the reason for using "hover" instead of "enter above"
        self.circle_go.leave_signal.connect(self._breathing_gi_leave)
        # ..text
        self.text_gi = TextGraphicsItem()
        self.text_gi.setAcceptHoverEvents(False)  # -so that the underlying item will not be disturbed
        help_text_str = "Hover over the green box breathing in and outside the green box breathing out"
        self.text_gi.setHtml(mc.globa.get_html(help_text_str, 16))
        self.text_gi.setTextWidth(VIEW_WIDTH_INT - 20)
        self.text_gi.setDefaultTextColor(QtGui.QColor(mc.globa.MC_DARKER_GREEN_COLOR_STR))
        text_pointf = QtCore.QPointF(
            VIEW_WIDTH_INT / 2 - self.text_gi.boundingRect().width() / 2,
            VIEW_HEIGHT_INT - self.text_gi.boundingRect().height()
        )
        self.text_gi.setPos(text_pointf)
        self._graphics_scene.addItem(self.text_gi)

        # Animation
        self.ib_qtimeline = QtCore.QTimeLine(duration=TIME_LINE_IB_DURATION_INT)
        self.ib_qtimeline.setFrameRange(1, TIME_LINE_IB_FRAME_RANGE_INT)
        self.ib_qtimeline.setCurveShape(QtCore.QTimeLine.LinearCurve)
        self.ib_qtimeline.frameChanged.connect(self.frame_change_breathing_in)
        self.ob_qtimeline = QtCore.QTimeLine(duration=TIME_LINE_OB_DURATION_INT)
        self.ob_qtimeline.setFrameRange(1, TIME_LINE_OB_FRAME_RANGE_INT)
        self.ob_qtimeline.setCurveShape(QtCore.QTimeLine.LinearCurve)
        self.ob_qtimeline.frameChanged.connect(self.frame_change_breathing_out)

        # https://forum.qt.io/topic/106003/how-to-seamlessly-place-item-into-scene-at-specific-location-adding-qgraphicsitem-to-scene-always-places-it-at-0-0/2

    def on_rest_button_clicked(self):
        self.rest_btn_clicked_signal.emit()

    # overridden
    def mousePressEvent(self, event: QtGui.QMouseEvent) -> None:
        super().mousePressEvent(event)
        if self._rest_button_gi.sceneBoundingRect().contains(event.pos()):
            # super().mouseReleaseEvent(event)
            event.ignore()
            return
        event.accept()
        if self._can_be_closed_bool:
            self.mouse_btn_released_signal.emit()

    # overridden
    def leaveEvent(self, i_qevent) -> None:
        if self._can_be_closed_bool:
            self.mouse_leave_signal.emit()

    def _start_breathing_in(self) -> None:
        ib_phrase: str = ""
        if mc.globa.active_phrase_id_it != mc.globa.BREATHING_PHRASE_NOT_SHOWN:
            # ib_phrase: str = mc.globa.get_breathing_phrase(mc.globa.active_phrase_id_it).in_breath
            ib_phrase: str = "Breathing in I know I am breathing in"
        self.text_gi.setHtml(mc.globa.get_html(ib_phrase, 18))
        self.ob_qtimeline.stop()
        self.ib_qtimeline.start()

    def _start_breathing_out(self) -> None:
        # phrase = mc.model.PhrasesM.get(mc.globa.active_phrase_id_it)
        ob_phrase: str = ""
        if mc.globa.active_phrase_id_it != mc.globa.BREATHING_PHRASE_NOT_SHOWN:
            # br_phrases: dict = mc.globa.settings[mc.globa.SK_BREATHING_PHRASES]
            # ob_phrase: str = mc.globa.get_breathing_phrase(mc.globa.active_phrase_id_it).out_breath
            ob_phrase: str = "Breathing out I know I am breathing out"
        self.text_gi.setHtml(mc.globa.get_html(ob_phrase, 18))
        self.ib_qtimeline.stop()
        self.ob_qtimeline.start()

    # overridden
    def keyPressEvent(self, i_qkeyevent) -> None:
        if self._keyboard_active_bool and i_qkeyevent.key() == QtCore.Qt.Key_Shift:
            logging.debug("shift key pressed")
            self._start_breathing_in()

    # overridden
    def keyReleaseEvent(self, i_qkeyevent) -> None:
        if self._keyboard_active_bool and i_qkeyevent.key() == QtCore.Qt.Key_Shift:
            logging.debug("shift key released")
            self._start_breathing_out()

    def _breathing_gi_hover(self) -> None:
        if mc.globa.breathing_state == mc.globa.BreathingState.breathing_in:
            return
        self.breathing_count_int += 1
        if self.breathing_count_int == 1:
            self.first_breathing_gi_signal.emit()

        # Calculating coords..
        # ..breathing rectangle/area
        hover_rectangle_qsize = QtCore.QSizeF(BR_WIDTH_FT, BR_HEIGHT_FT)
        x: float = self.circle_go.x() + (self.circle_go.boundingRect().width() - hover_rectangle_qsize.width()) / 2
        y: float = self.circle_go.y() + (self.circle_go.boundingRect().height() - hover_rectangle_qsize.height()) / 2
        pos_pointf = QtWidgets.QGraphicsItem.mapFromItem(self.circle_go, self.circle_go, x, y)  # -widget coords
        hover_rectangle_coords_qrect = QtCore.QRectF(pos_pointf, hover_rectangle_qsize)
        logging.debug(f"{hover_rectangle_coords_qrect=}")
        # ..mouse cursor
        cursor = QtGui.QCursor()  # -screen coords
        cursor_pos_widget_coords_qp = self.mapFromGlobal(cursor.pos())  # -widget coords
        logging.debug(f"{cursor_pos_widget_coords_qp=}")

        # Check if cursor is inside the area, and start the animation
        if hover_rectangle_coords_qrect.contains(cursor_pos_widget_coords_qp):
            mc.globa.breathing_state = mc.globa.BreathingState.breathing_in
            self._start_breathing_in()
            self.circle_go.update_pos_and_origin_point(VIEW_WIDTH_INT, VIEW_HEIGHT_INT)

    def _breathing_gi_leave(self) -> None:
        if mc.globa.breathing_state != mc.globa.BreathingState.breathing_in:
            return
        mc.globa.breathing_state = mc.globa.BreathingState.breathing_out

        self._peak_scale_ft = self.circle_go.scale()
        self._start_breathing_out()
        # self.text_gi.update_pos_and_origin_point(VIEW_WIDTH_INT, VIEW_HEIGHT_INT)
        self.circle_go.update_pos_and_origin_point(VIEW_WIDTH_INT, VIEW_HEIGHT_INT)

    def frame_change_breathing_in(self, i_frame_nr_int: int) -> None:
        new_scale_int_ft = 1 + 0.001 * i_frame_nr_int
        self.circle_go.setScale(new_scale_int_ft)

    def frame_change_breathing_out(self, i_frame_nr_int: int) -> None:
        new_scale_int_ft = self._peak_scale_ft - 0.0005 * i_frame_nr_int
        if new_scale_int_ft < MIN_SCALE_FT:
            new_scale_int_ft = MIN_SCALE_FT
        self.circle_go.setScale(new_scale_int_ft)


class TextGraphicsItem(QtWidgets.QGraphicsTextItem):
    def __init__(self):
        super().__init__()


class BreathingCircleGraphicsObject(QtWidgets.QGraphicsObject):
    hover_signal = QtCore.pyqtSignal()
    leave_signal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()
        self.rectf = QtCore.QRectF(0.0, 0.0, BR_WIDTH_FT, BR_HEIGHT_FT)
        self.setAcceptHoverEvents(True)

    # Overridden
    def paint(self, i_qpainter: QtGui.QPainter, i_qstyleoptiongraphicsitem, widget=None) -> None:
        # i_qpainter.fillRect(self.rectf, t_brush)
        t_brush = QtGui.QBrush(QtGui.QColor(mc.globa.MC_LIGHT_GREEN_COLOR_STR))
        i_qpainter.setBrush(t_brush)
        i_qpainter.drawEllipse(self.rectf)

    # Overridden
    def boundingRect(self):
        return self.rectf

    # Overridden
    def hoverMoveEvent(self, i_qgraphicsscenehoverevent: QtWidgets.QGraphicsSceneHoverEvent) -> None:
        self.hover_signal.emit()
        """
        cposx = self.boundingRect().center().x()
        cposy = self.boundingRect().center().y()
        pposx = i_qgraphicsscenehoverevent.pos().x()
        pposy = i_qgraphicsscenehoverevent.pos().y()

        distance_from_center: float = math.dist([cposx, cposy], [pposx, pposy])
        # logging.debug(f"{distance_from_center=}")

        # TODO: reading https://doc.qt.io/qt-5/qgraphicsitem.html#details

        if distance_from_center < BR_WIDTH_FT:
            self.hover_signal.emit()
        else:
            self.leave_signal.emit()
        """

    # Overridden
    def hoverLeaveEvent(self, i_qgraphicsscenehoverevent) -> None:
        # Please note that this function is entered in case the user hovers over something
        #  on top of this graphics item
        self.leave_signal.emit()

    def update_pos_and_origin_point(self, i_view_width: int, i_view_height: int) -> None:
        x: float = i_view_width / 2 - self.boundingRect().width() / 2
        y: float = i_view_height / 2 - self.boundingRect().height() / 2
        self.setPos(QtCore.QPointF(x, y))
        self.setTransformOriginPoint(self.boundingRect().center())


class RestButtonGraphicsObject(QtWidgets.QGraphicsWidget):
    clicked_signal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()
        self.rectf = QtCore.QRectF(0.0, 0.0, REST_BTN_SIZE, REST_BTN_SIZE)
        """
        https://doc.qt.io/qt-5/qgraphicsitem.html#acceptedMouseButtons

        If an item accepts a mouse button, it will become the mouse grabber item when a
        mouse press event is delivered for that mouse button. However, if the item does
        not accept the button, QGraphicsScene will forward the mouse events to the first
        item beneath it that does.
        
        https://doc.qt.io/qt-5/qgraphicsitem.html#mousePressEvent

        The mouse press event decides which item should become the mouse grabber (see
        QGraphicsScene::mouseGrabberItem()). If you do not reimplement this function,
        the press event will propagate to any topmost item beneath this item, *and no
        other mouse events will be delivered to this item*.
        
        If you do reimplement this function, event will by default be accepted (see
        QEvent::accept()), and this item is then the mouse grabber. This allows the
        item to receive future move, release and doubleclick events. If you call
        QEvent::ignore() on event, this item will lose the mouse grab, and event will
        propagate to any topmost item beneath. No further mouse events will be
        delivered to this item unless a new mouse press event is received.
        """
        # QtCore.Qt.ItemIsSelectable
        # self.setAcceptedMouseButtons(True)
        # self.grabMouseEvent()

    # Overridden
    def paint(self, i_qpainter, i_qstyleoptiongraphicsitem, widget=None) -> None:
        t_brush = QtGui.QBrush(QtGui.QColor(mc.globa.MC_DARK_GREEN_COLOR_STR))
        i_qpainter.fillRect(self.rectf, t_brush)

    # Overridden
    def boundingRect(self):
        return self.rectf

    def mousePressEvent(self, event: QtWidgets.QGraphicsSceneMouseEvent) -> None:
        self.clicked_signal.emit()
        event.accept()
