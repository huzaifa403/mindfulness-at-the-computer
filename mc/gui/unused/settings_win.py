import logging
import sys
import random
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
import mc.gui.rest_action_list_wt
import mc.globa
import mc.gui.breathing_settings_wt
import mc.gui.timing_settings_wt
import mc.gui.breathing_phrase_list_wt
import mc.gui.general_settings_wt
import mc.gui.rest_settings_wt
# import mc.gui.intro_dlg
import mc.gui.feedback_dlg
import mc.gui.sysinfo_dlg


class SettingsWin(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.central_tabbed_wt = QtWidgets.QTabWidget()

        # self.active_breathing_phrase_qgb = QtWidgets.QGroupBox("Active Breathing Phrase")

        self.setGeometry(100, 64, 900, 670)
        self.setWindowIcon(QtGui.QIcon(mc.globa.get_app_icon_path("icon.png")))

        if mc.globa.testing_bool:
            data_storage_str = "{Testing - data stored in memory}"
        else:
            data_storage_str = "{Live - data stored on hard drive}"
        window_title_str = (
            mc.globa.APPLICATION_TITLE_STR
            + " [" + mc.globa.APPLICATION_VERSION_STR + "] "
            + data_storage_str
        )
        self.setWindowTitle(window_title_str)

        self.setStyleSheet(
            "selection-background-color:" + mc.globa.MC_LIGHT_GREEN_COLOR_STR + ";"
            "selection-color:#000000;"
        )

        self.setCentralWidget(self.central_tabbed_wt)

        self.breathing_settings_wt = mc.gui.breathing_settings_wt.BreathingSettingsWt()
        self.rest_settings_wt = mc.gui.rest_settings_wt.RestSettingsWt()
        self.timing_settings_wt = mc.gui.timing_settings_wt.TimingSettingsWt()
        self.general_settings_wt = mc.gui.general_settings_wt.GeneralSettingsWt()

        self.central_tabbed_wt.setTabPosition(QtWidgets.QTabWidget.West)

        self.central_tabbed_wt.addTab(self.breathing_settings_wt, self.tr("Breathing"))
        self.central_tabbed_wt.addTab(self.rest_settings_wt, self.tr("Resting"))
        self.central_tabbed_wt.addTab(self.timing_settings_wt, self.tr("Timers"))
        if QtCore.QSysInfo.kernelType() == 'darwin':
            self.central_tabbed_wt.addTab(self.general_settings_wt, self.tr("General settings"))


        self.breathing_settings_wt.phrases_qlw.selection_changed_signal.connect(
            self.on_breathing_list_row_changed
        )
        self.breathing_settings_wt.phrases_qlw.phrase_changed_signal.connect(
            self.on_breathing_phrase_changed
        )
        # self.rest_settings_wt.phrases_qlw.timeout_signal.connect(self.on_rest_action_list_updated)
        self.rest_settings_wt.phrases_qlw.selection_changed_signal.connect(
            self.on_rest_action_list_row_changed
        )
        self.rest_settings_wt.settings_updated_signal.connect(
            self.on_rest_settings_changed
        )
        self.breathing_settings_wt.updated_signal.connect(self.on_breathing_settings_changed)
        self.timing_settings_wt.breathing_settings_updated_from_settings_signal.connect(
            self.on_breathing_settings_changed
        )

        # Setup of Menu
        self.menu_bar = self.menuBar()
        self.update_menu()

    def on_rest_action_list_updated(self):
        self.update_gui(mc.globa.EventSource.rest_action_changed)

    def on_breathing_list_row_changed(self, i_details_enabled: bool):
        # self.update_breathing_timer()
        self.breathing_settings_wt.setEnabled(i_details_enabled)
        # self.sys_tray.breathing_enabled_qaction.setEnabled(i_details_enabled)

        self.update_gui(mc.globa.EventSource.breathing_list_selection_changed)

    def on_rest_action_list_row_changed(self):
        self.update_gui(mc.globa.EventSource.rest_list_selection_changed)

    def on_breathing_phrase_changed(self, i_details_enabled):
        self.update_breathing_timer()
        self.breathing_settings_wt.setEnabled(i_details_enabled)
        self.sys_tray.breathing_enabled_qaction.setEnabled(i_details_enabled)

        self.update_gui(mc.globa.EventSource.breathing_list_phrase_updated)

    def update_rest_timer(self, origin=None):
        if origin:
            logging.debug("Rest timer updated from " + origin)
        settings = mc.model.SettingsM.get()
        if settings.rest_reminder_active:
            self.start_rest_timer()
        else:
            self.stop_rest_timer()
        self.update_tooltip()
        if origin == 'intro':
            self.update_gui(mc.globa.EventSource.rest_settings_changed_from_intro)
        else:
            self.update_gui(mc.globa.EventSource.rest_settings_changed_from_settings)

    def restore_window(self):
        self.raise_()
        self.showNormal()
        # another alternative (from an SO answer): self.setWindowState(QtCore.Qt.WindowActive)

    def on_rest_settings_changed(self):
        # self.update_rest_timer()
        self.update_gui(mc.globa.EventSource.rest_settings_changed_from_settings)

    def on_breathing_settings_changed(self, origin=None):
        self.update_breathing_timer()

        if origin == 'intro':
            self.update_gui(mc.globa.EventSource.breathing_settings_changed_from_intro)
        else:
            self.update_gui(mc.globa.EventSource.breathing_settings_changed_from_settings)

    def update_menu(self):
        self.menu_bar.clear()

        file_menu = self.menu_bar.addMenu(self.tr("&File"))
        minimize_to_tray_action = QtWidgets.QAction(self.tr("Minimize to tray"), self)
        file_menu.addAction(minimize_to_tray_action)
        minimize_to_tray_action.triggered.connect(self.minimize_to_tray)
        """
        choose_file_directory_action = QtWidgets.QAction(self.tr("Choose file directory"), self)
        file_menu.addAction(choose_file_directory_action)
        choose_file_directory_action.triggered.connect(pass)
        """
        quit_action = QtWidgets.QAction(self.tr("Quit"), self)
        file_menu.addAction(quit_action)
        quit_action.triggered.connect(self.exit_application)

        debug_menu = self.menu_bar.addMenu(self.tr("&Debug"))
        update_gui_action = QtWidgets.QAction(self.tr("Update GUI"), self)
        debug_menu.addAction(update_gui_action)
        update_gui_action.triggered.connect(self.update_gui)
        breathing_full_screen_action = QtWidgets.QAction(self.tr("Full screen"), self)
        debug_menu.addAction(breathing_full_screen_action)
        breathing_full_screen_action.triggered.connect(self.showFullScreen)

        help_menu = self.menu_bar.addMenu(self.tr("&Help"))
        show_intro_dialog_action = QtWidgets.QAction(self.tr("Show intro wizard"), self)
        help_menu.addAction(show_intro_dialog_action)
        show_intro_dialog_action.triggered.connect(self.show_intro_dialog)
        about_action = QtWidgets.QAction(self.tr("About"), self)
        help_menu.addAction(about_action)
        about_action.triggered.connect(self.show_about_box)
        online_help_action = QtWidgets.QAction(self.tr("Online help"), self)
        help_menu.addAction(online_help_action)
        online_help_action.triggered.connect(self.show_online_help)
        feedback_action = QtWidgets.QAction(self.tr("Give feedback"), self)
        help_menu.addAction(feedback_action)
        feedback_action.triggered.connect(self.show_feedback_dialog)
        sysinfo_action = QtWidgets.QAction(self.tr("System Information"), self)
        help_menu.addAction(sysinfo_action)
        sysinfo_action.triggered.connect(self.show_sysinfo_box)

    def show_feedback_dialog(self):
        feedback_dlg = mc.gui.feedback_dlg.FeedbackDialog()
        feedback_dlg.exec_()

    def show_intro_dialog(self):
        self.intro_dlg = mc.gui.intro_dlg.IntroDlg()
        self.intro_dlg.initial_setup.breathing_settings_updated_from_intro_signal.connect(
            self.on_breathing_settings_changed
        )
        """
        self.intro_dlg.initial_setup.rest_settings_updated_from_intro_signal.connect(
            self.update_rest_timer
        )
        """
        self.intro_dlg.close_signal.connect(self.on_intro_dialog_closed)
        self.intro_dlg.exec()
        self.intro_dlg.initial_setup.update_gui()
        self.update_gui()

    def on_intro_dialog_closed(self, i_open_breathing_dialog: bool):
        if i_open_breathing_dialog:
            self.open_breathing_dialog()

    def on_breathing_dialog_phrase_changed(self):
        self.update_gui()

    def on_breathing_notification_breathe_clicked(self):
        self.open_breathing_dialog(i_mute_override=True)

    def debug_clear_breathing_phrase_selection(self):
        self.br_phrase_list_wt.list_widget.clearSelection()

    def show_online_help(self):
        url_str = "https://mindfulness-at-the-computer.gitlab.io/user_guide"
        # noinspection PyCallByClass
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url_str))
        # Python: webbrowser.get(url_str) --- doesn't work

    def show_sysinfo_box(self):
        self._sysinfo_dlg = mc.gui.sysinfo_dlg.SysinfoDialog()
        self._sysinfo_dlg.show()

    def show_about_box(self):
        created_by_str = (
            '<p>Originally created by Tord Dellsén - '
            '<a href="https://sunyatazero.gitlab.io/">Website</a></p>'
        )
        all_contributors_str = (
            '<p><a href="https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/graphs/master">'
            'All contributors</a></p>'
        )
        photography_str = (
            '<p>Photography for application icon by Torgny Dellsén - '
            '<a href="http://torgnydellsen.zenfolio.com">torgnydellsen.zenfolio.com</a></p>'
        )
        open_iconic_str = (
            '<p>Other icons from Open Iconic - useiconic.com - MIT license</p>'
        )
        about_html_str = (
            '<html>'
            + created_by_str
            + all_contributors_str
            + photography_str
            + open_iconic_str
            + '<p>Other images (for the rest actions) have been released into the public domain (CC0)</p>'
            + '<p>All audio files used have been released into the public domain (CC0)</p>'
            + '<p>Software License: GPLv3 (license text available in the install directory)</p>'
            + '</html>'
        )

        # noinspection PyCallByClass
        QtWidgets.QMessageBox.about(self, "About Mindfulness at the Computer", about_html_str)

    # overridden
    # noinspection PyPep8Naming
    def closeEvent(self, i_QCloseEvent):
        i_QCloseEvent.ignore()
        self.minimize_to_tray()

    def minimize_to_tray(self):
        self.showMinimized()
        self.hide()

    def exit_application(self):
        sys.exit()

    def update_gui(self, i_event_source=mc.globa.EventSource.undefined):
        if mc.globa.active_phrase_id_it == mc.globa.NO_PHRASE_SELECTED_INT:
            logging.error("[Error] No phrase selected")
            raise RuntimeError("[RuntimeError] No phrase has been selected")
        else:
            # breathing_phrase = mc.model.PhrasesM.get(mc.globa.active_phrase_id_it)
            # self.title_text_qll.setText(breathing_phrase.title)
            # self.in_text_qll.setText(breathing_phrase.ib)
            # self.out_text_qll.setText(breathing_phrase.ob)

            if i_event_source != mc.globa.EventSource.rest_slider_value_changed:
                self.rest_settings_wt.update_gui()
                self.timing_settings_wt.update_gui()
            self.breathing_settings_wt.update_gui()

            if (i_event_source != mc.globa.EventSource.breathing_list_selection_changed
                and i_event_source != mc.globa.EventSource.rest_list_selection_changed):
                self.breathing_settings_wt.phrases_qlw.update_gui()
                self.rest_settings_wt.phrases_qlw.update_gui()

            self.update_systray()

        # Update the timers pages in intro or settings
        if (i_event_source == mc.globa.EventSource.breathing_settings_changed_from_intro or
                i_event_source == mc.globa.EventSource.rest_settings_changed_from_intro):
            logging.debug("Updating settings page because settings have changed from intro")
            self.timing_settings_wt.update_gui()

        if (self.intro_dlg and
                (i_event_source == mc.globa.EventSource.breathing_settings_changed_from_settings or
                 i_event_source == mc.globa.EventSource.rest_settings_changed_from_settings)):
            logging.debug("Updating intro page because settings have changed from settings")
            self.intro_dlg.initial_setup.update_gui()

