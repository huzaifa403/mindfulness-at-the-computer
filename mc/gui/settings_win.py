import logging
import sys
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
import mc.globa


class SettingsWin(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.central_tabbed_wt = QtWidgets.QTabWidget()
        self.setGeometry(100, 64, 900, 670)
        self.setWindowIcon(QtGui.QIcon(mc.globa.get_app_icon_path("icon.png")))

        if mc.globa.testing_bool:
            data_storage_str = "{Testing - data stored in memory}"
        else:
            data_storage_str = "{Live - data stored on hard drive}"
        window_title_str = (
            mc.globa.APPLICATION_TITLE_STR
            + " [" + mc.globa.APPLICATION_VERSION_STR + "] "
            + data_storage_str
        )
        self.setWindowTitle(window_title_str)

        self.setStyleSheet(
            "selection-background-color:" + mc.globa.MC_LIGHT_GREEN_COLOR_STR + ";"
            "selection-color:#000000;"
        )

        self.setCentralWidget(self.central_tabbed_wt)


        self.general_settings_wt = GeneralSettingsWt()
        self.central_tabbed_wt.addTab(self.general_settings_wt, "General")
        self.breathing_settings_wt = BreathingSettingsWt()
        self.central_tabbed_wt.addTab(self.breathing_settings_wt, "Breathing")
        self.rest_settings_wt = RestSettingsWt()
        self.central_tabbed_wt.addTab(self.rest_settings_wt, "Rest")
        # self.timing_settings_wt = mc.gui.timing_settings_wt.TimingSettingsWt()

        # Setup of Menu
        self.menu_bar = self.menuBar()
        self.update_menu()

    def on_breathing_list_row_changed(self, i_details_enabled: bool):
        # self.update_breathing_timer()
        self.breathing_settings_wt.setEnabled(i_details_enabled)
        # self.sys_tray.breathing_enabled_qaction.setEnabled(i_details_enabled)

        self.update_gui(mc.globa.EventSource.breathing_list_selection_changed)

    def on_rest_action_list_row_changed(self):
        self.update_gui(mc.globa.EventSource.rest_list_selection_changed)

    def on_breathing_phrase_changed(self, i_details_enabled):
        self.update_breathing_timer()
        self.breathing_settings_wt.setEnabled(i_details_enabled)
        self.sys_tray.breathing_enabled_qaction.setEnabled(i_details_enabled)

        self.update_gui(mc.globa.EventSource.breathing_list_phrase_updated)

    def update_menu(self):
        self.menu_bar.clear()

        file_menu = self.menu_bar.addMenu(self.tr("&File"))
        minimize_to_tray_action = QtWidgets.QAction(self.tr("Minimize to tray"), self)
        file_menu.addAction(minimize_to_tray_action)
        minimize_to_tray_action.triggered.connect(self.minimize_to_tray)
        """
        choose_file_directory_action = QtWidgets.QAction(self.tr("Choose file directory"), self)
        file_menu.addAction(choose_file_directory_action)
        choose_file_directory_action.triggered.connect(pass)
        """
        quit_action = QtWidgets.QAction(self.tr("Quit"), self)
        file_menu.addAction(quit_action)
        quit_action.triggered.connect(self.exit_application)

        debug_menu = self.menu_bar.addMenu(self.tr("&Debug"))
        update_gui_action = QtWidgets.QAction(self.tr("Update GUI"), self)
        debug_menu.addAction(update_gui_action)
        update_gui_action.triggered.connect(self.update_gui)
        breathing_full_screen_action = QtWidgets.QAction(self.tr("Full screen"), self)
        debug_menu.addAction(breathing_full_screen_action)
        breathing_full_screen_action.triggered.connect(self.showFullScreen)

        help_menu = self.menu_bar.addMenu(self.tr("&Help"))
        show_intro_dialog_action = QtWidgets.QAction(self.tr("Show intro wizard"), self)
        help_menu.addAction(show_intro_dialog_action)
        show_intro_dialog_action.triggered.connect(self.show_intro_dialog)
        about_action = QtWidgets.QAction(self.tr("About"), self)
        help_menu.addAction(about_action)
        about_action.triggered.connect(self.show_about_box)
        online_help_action = QtWidgets.QAction(self.tr("Online help"), self)
        help_menu.addAction(online_help_action)
        online_help_action.triggered.connect(self.show_online_help)
        feedback_action = QtWidgets.QAction(self.tr("Give feedback"), self)
        help_menu.addAction(feedback_action)
        feedback_action.triggered.connect(self.show_feedback_dialog)
        sysinfo_action = QtWidgets.QAction(self.tr("System Information"), self)
        help_menu.addAction(sysinfo_action)
        sysinfo_action.triggered.connect(self.show_sysinfo_box)

    def show_feedback_dialog(self):
        feedback_dlg = mc.gui.feedback_dlg.FeedbackDialog()
        feedback_dlg.exec_()

    def show_intro_dialog(self):
        self.intro_dlg = mc.gui.intro_dlg.IntroDlg()
        self.intro_dlg.initial_setup.breathing_settings_updated_from_intro_signal.connect(
            self.on_breathing_settings_changed
        )
        """
        self.intro_dlg.initial_setup.rest_settings_updated_from_intro_signal.connect(
            self.update_rest_timer
        )
        """
        self.intro_dlg.close_signal.connect(self.on_intro_dialog_closed)
        self.intro_dlg.exec()
        self.intro_dlg.initial_setup.update_gui()
        self.update_gui()

    def on_intro_dialog_closed(self, i_open_breathing_dialog: bool):
        if i_open_breathing_dialog:
            self.open_breathing_dialog()

    def on_breathing_dialog_phrase_changed(self):
        self.update_gui()

    def on_breathing_notification_breathe_clicked(self):
        self.open_breathing_dialog(i_mute_override=True)

    def debug_clear_breathing_phrase_selection(self):
        self.br_phrase_list_wt.list_widget.clearSelection()

    def show_online_help(self):
        url_str = "https://mindfulness-at-the-computer.gitlab.io/user_guide"
        # noinspection PyCallByClass
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url_str))
        # Python: webbrowser.get(url_str) --- doesn't work

    def show_sysinfo_box(self):
        self._sysinfo_dlg = mc.gui.sysinfo_dlg.SysinfoDialog()
        self._sysinfo_dlg.show()

    def show_about_box(self):
        created_by_str = (
            '<p>Originally created by Tord Dellsén - '
            '<a href="https://sunyatazero.gitlab.io/">Website</a></p>'
        )
        all_contributors_str = (
            '<p><a href="https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/graphs/master">'
            'All contributors</a></p>'
        )
        photography_str = (
            '<p>Photography for application icon by Torgny Dellsén - '
            '<a href="http://torgnydellsen.zenfolio.com">torgnydellsen.zenfolio.com</a></p>'
        )
        open_iconic_str = (
            '<p>Other icons from Open Iconic - useiconic.com - MIT license</p>'
        )
        about_html_str = (
            '<html>'
            + created_by_str
            + all_contributors_str
            + photography_str
            + open_iconic_str
            + '<p>Other images (for the rest actions) have been released into the public domain (CC0)</p>'
            + '<p>All audio files used have been released into the public domain (CC0)</p>'
            + '<p>Software License: GPLv3 (license text available in the install directory)</p>'
            + '</html>'
        )

        # noinspection PyCallByClass
        QtWidgets.QMessageBox.about(self, "About Mindfulness at the Computer", about_html_str)

    # overridden
    # noinspection PyPep8Naming
    def closeEvent(self, i_QCloseEvent):
        i_QCloseEvent.ignore()
        self.minimize_to_tray()

    def minimize_to_tray(self):
        self.showMinimized()
        self.hide()

    def exit_application(self):
        QtWidgets.QApplication.quit()
        # sys.exit()

    def update_gui(self, i_event_source=mc.globa.EventSource.undefined):
        if mc.globa.active_phrase_id_it == mc.globa.NO_PHRASE_SELECTED_INT:
            logging.error("[Error] No phrase selected")
            raise RuntimeError("[RuntimeError] No phrase has been selected")
        else:
            # breathing_phrase = mc.model.PhrasesM.get(mc.globa.active_phrase_id_it)
            # self.title_text_qll.setText(breathing_phrase.title)
            # self.in_text_qll.setText(breathing_phrase.ib)
            # self.out_text_qll.setText(breathing_phrase.ob)

            if i_event_source != mc.globa.EventSource.rest_slider_value_changed:
                self.rest_settings_wt.update_gui()
                self.timing_settings_wt.update_gui()
            self.breathing_settings_wt.update_gui()

            if (i_event_source != mc.globa.EventSource.breathing_list_selection_changed
                and i_event_source != mc.globa.EventSource.rest_list_selection_changed):
                self.breathing_settings_wt.phrases_qlw.update_gui()
                self.rest_settings_wt.phrases_qlw.update_gui()

            self.update_systray()

        # Update the timers pages in intro or settings
        if (i_event_source == mc.globa.EventSource.breathing_settings_changed_from_intro or
                i_event_source == mc.globa.EventSource.rest_settings_changed_from_intro):
            logging.debug("Updating settings page because settings have changed from intro")
            self.timing_settings_wt.update_gui()

        if (self.intro_dlg and
                (i_event_source == mc.globa.EventSource.breathing_settings_changed_from_settings or
                 i_event_source == mc.globa.EventSource.rest_settings_changed_from_settings)):
            logging.debug("Updating intro page because settings have changed from settings")
            self.intro_dlg.initial_setup.update_gui()


class SettingsBaseWt(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.updating_gui: bool = True

        self.hbox_l2 = QtWidgets.QHBoxLayout(self)
        self.setLayout(self.hbox_l2)

        self.vbox_left_l3 = QtWidgets.QVBoxLayout()
        self.hbox_l2.addLayout(self.vbox_left_l3)

        self.vbox_right_l3 = QtWidgets.QVBoxLayout()
        self.hbox_l2.addLayout(self.vbox_right_l3)


class GeneralSettingsWt(SettingsBaseWt):
    def __init__(self):
        super().__init__()


class BreathingSettingsWt(SettingsBaseWt):
    def __init__(self):
        super().__init__()

        self.breathing_break_time_qsb = QtWidgets.QSpinBox()
        self.vbox_left_l3.addWidget(self.breathing_break_time_qsb)
        self.breathing_break_time_qsb.valueChanged.connect(self.on_br_time_value_changed)

        # Initial setup

        # self.update_gui()

        self.updating_gui: bool = False

    def on_br_time_value_changed(self, i_new_value: int):
        if self.updating_gui:
            return
        mc.globa.settings[mc.globa.SK_BREATHING_BREAK_TIMER_SECS] = i_new_value

    def update_gui(self):
        self.updating_gui = True

        br_time_value: int = mc.globa.settings[mc.globa.SK_BREATHING_BREAK_TIMER_SECS]
        self.breathing_break_time_qsb.setValue(br_time_value)

        self.update_gui()


class RestSettingsWt(SettingsBaseWt):
    def __init__(self):
        super().__init__()

        self.rest_actions_qlw = QtWidgets.QListWidget()
        self.vbox_right_l3.addWidget(self.rest_actions_qlw)

        list_edit_hbox_l4 = QtWidgets.QHBoxLayout()
        self.vbox_right_l3.addLayout(list_edit_hbox_l4)

        self.list_add_qpb = QtWidgets.QPushButton("Add")
        list_edit_hbox_l4.addWidget(self.list_add_qpb)
        self.list_add_qpb.clicked.connect(self.on_list_add_clicked)

        self.list_edit_qpb = QtWidgets.QPushButton("Edit")
        list_edit_hbox_l4.addWidget(self.list_edit_qpb)
        self.list_edit_qpb.clicked.connect(self.on_list_edit_clicked)

        self.list_remove_qpb = QtWidgets.QPushButton("Remove")
        list_edit_hbox_l4.addWidget(self.list_remove_qpb)
        self.list_remove_qpb.clicked.connect(self.on_list_remove_clicked)


        self.ra_add_dlg = RestActionSettingsDlg()
        self.ra_add_dlg.finished.connect(self.on_add_ra_dlg_finished)

        self.ra_edit_dlg = RestActionSettingsDlg()
        self.ra_edit_dlg.finished.connect(self.on_edit_ra_dlg_finished)



        self.update_gui()

    def on_list_remove_clicked(self):
        rest_actions: dict = mc.globa.settings[mc.globa.SK_REST_ACTIONS]
        current_qlwi = self.rest_actions_qlw.currentItem()
        id_key: str = current_qlwi.data(QtCore.Qt.UserRole)
        mc.globa.remove_dict_item(rest_actions, id_key)
        self.update_gui()

    def on_list_add_clicked(self):
        self.ra_edit_dlg = RestActionSettingsDlg()
        self.ra_edit_dlg.finished.connect(self.on_add_ra_dlg_finished)
        self.ra_add_dlg.exec()
        self.update_gui()

    def on_list_edit_clicked(self):
        current_qlwi = self.rest_actions_qlw.currentItem()
        id_key: int = (current_qlwi.data(QtCore.Qt.UserRole))
        # for qlwi in self.rest_actions_qlw.items():
        if id_key:
            self.ra_edit_dlg = RestActionSettingsDlg(id_key)
            self.ra_edit_dlg.finished.connect(self.on_edit_ra_dlg_finished)
            self.ra_edit_dlg.exec()
            print(f"{mc.globa.settings}")

    def on_add_ra_dlg_finished(self, i_result: int):
        if i_result == QtWidgets.QDialog.Accepted:
            rest_actions: dict = mc.globa.settings[mc.globa.SK_REST_ACTIONS]
            title: str = self.ra_add_dlg.ra_title_qle.text()
            # mc.globa.set_dict_sub_value(rest_actions, self.id_key, mc.globa.SK_REST_ACTIONS, title)
            mc.globa.add_dict_item(rest_actions, title, "/path/")
            self.update_gui()

    def on_edit_ra_dlg_finished(self, i_result: int):
        if i_result == QtWidgets.QDialog.Accepted:
            rest_actions: dict = mc.globa.settings[mc.globa.SK_REST_ACTIONS]
            title: str = self.ra_edit_dlg.ra_title_qle.text()
            current_item = self.rest_actions_qlw.currentItem()
            id_key = current_item.data(QtCore.Qt.UserRole)
            mc.globa.set_dict_sub_values(rest_actions, id_key,
                [(mc.globa.SP_RA_TITLE, title)]
            )
            #mc.globa.add_dict_item(rest_actions, title, "/path/")
            self.update_gui()

    def update_gui(self):
        self.updating_gui = True

        self.rest_actions_qlw.clear()
        rest_actions: list[mc.globa.RestAction] = mc.globa.settings.get(mc.globa.SK_REST_ACTIONS)
        for ra in rest_actions:
            qlwi = QtWidgets.QListWidgetItem(ra.title)
            qlwi.setData(QtCore.Qt.UserRole, ra.id)
            self.rest_actions_qlw.addItem(qlwi)

        self.updating_gui = False


class RestActionSettingsDlg(QtWidgets.QDialog):
    def __init__(self, i_id_key: int = -1):
        super().__init__()
        self.setModal(True)
        # -"Dialogs can be application modal (the default) or window modal."
        self.id_key: int = i_id_key

        vbox_l2 = QtWidgets.QVBoxLayout(self)

        rest_actions: list[mc.globa.RestAction] = mc.globa.settings[mc.globa.SK_REST_ACTIONS]

        self.ra_title_qle = QtWidgets.QLineEdit()
        vbox_l2.addWidget(self.ra_title_qle)
        # self.ra_title_qle.textChanged.connect(self.on_ra_title_text_changed)

        self.ra_path = QtWidgets.QLabel()
        vbox_l2.addWidget(self.ra_path)

        ra_title: str = ""
        if self.id_key != -1:
            ra = mc.globa.get_rest_action(self.id_key)
            self.ra_title_qle.setText(ra_title)
            self.ra_path.setText(ra.image_path)

        self.button_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel,
            QtCore.Qt.Horizontal, self
        )
        vbox_l2.addWidget(self.button_box)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

    """
    def on_ra_title_text_changed(self, i_new_text: str):
        rest_actions: dict = mc.globa.settings[mc.globa.SK_REST_ACTIONS]
        mc.globa.set_dict_sub_value(
            rest_actions, self.id_key, mc.globa.SK_REST_ACTIONS, i_new_text
        )
    """
