import sys
import logging
import enum
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
try:
    # noinspection PyUnresolvedReferences
    from PyQt5 import QtMultimedia
except ImportError:
    logging.debug("ImportError for QtMultimedia - maybe because there's no sound card available")
    # -If the system does not have a sound card (as for example Travis CI)
    # -An alternative to this approach is to use this: http://doc.qt.io/qt-5/qaudiodeviceinfo.html#availableDevices
import mc.globa
import mc.gui.breathing_and_rest_dlg
import mc.gui.settings_win


class MainObject(QtCore.QObject):
    """
    We are using this QObject as the core of the application (rather than using a QMainWindow)
    Areas of responsibility:
    * Breathing timer
    * Holds the settings window
    * Holds the breathing-and-rest dialog
    * Handles audio
    * Holds the systray

    """
    def __init__(self):
        super().__init__()

        # System tray and menu setup
        self.tray_icon = QtWidgets.QSystemTrayIcon(self)
        self.tray_icon.show()
        self.tray_menu = QtWidgets.QMenu()  # self
        self.tray_menu.aboutToShow.connect(self.on_tray_menu_about_to_show)
        self.rest_progress_qaction = QtWidgets.QAction("")
        self.tray_menu.addAction(self.rest_progress_qaction)
        self.rest_progress_qaction.setDisabled(True)
        self.tray_rest_now_qaction = QtWidgets.QAction(self.tr("Rest Dialog"))
        self.tray_menu.addAction(self.tray_rest_now_qaction)
        self.tray_rest_now_qaction.triggered.connect(self.on_tray_rest_now_triggered)
        self.tray_open_breathing_dialog_qaction = QtWidgets.QAction(self.tr("Breathing Dialog"))
        self.tray_menu.addAction(self.tray_open_breathing_dialog_qaction)
        self.tray_open_breathing_dialog_qaction.triggered.connect(self.on_tray_open_breathing_dialog_triggered)
        self.tray_menu.addSeparator()
        # self.tray_suspend_action = QtWidgets.QAction(self.tr("Suspend Application"))
        # self.tray_menu.addAction(self.tray_suspend_action)
        # self.tray_suspend_action.triggered.connect(self.on_suspend_application_clicked)
        self.tray_open_settings_action = QtWidgets.QAction(self.tr("Settings"))
        self.tray_menu.addAction(self.tray_open_settings_action)
        self.tray_open_settings_action.triggered.connect(self.on_tray_open_settings_triggered)
        self.tray_quit_action = QtWidgets.QAction(self.tr("Quit"))
        self.tray_menu.addAction(self.tray_quit_action)
        self.tray_quit_action.triggered.connect(self.on_tray_quit_triggered)
        self.tray_icon.setContextMenu(self.tray_menu)
        self.tray_menu.setDefaultAction(self.tray_open_breathing_dialog_qaction)
        self.update_systray_image()

        # System info (more is available in main.py)
        systray_available_str = "No"
        if self.tray_icon.isSystemTrayAvailable():
            systray_available_str = "Yes"
        mc.globa.sys_info_telist.append(("System tray available", systray_available_str))
        notifications_supported_str = "No"
        if self.tray_icon.supportsMessages():
            notifications_supported_str = "Yes"
        mc.globa.sys_info_telist.append(("System tray notifications supported", notifications_supported_str))
        sys_info = QtCore.QSysInfo()
        mc.globa.sys_info_telist.append(("buildCpuArchitecture", sys_info.buildCpuArchitecture()))
        mc.globa.sys_info_telist.append(("currentCpuArchitecture", sys_info.currentCpuArchitecture()))
        mc.globa.sys_info_telist.append(("Kernel type", sys_info.kernelType()))
        mc.globa.sys_info_telist.append(("Kernel version", sys_info.kernelVersion()))
        mc.globa.sys_info_telist.append(("Product name and version", sys_info.prettyProductName()))
        logging.info("##### System Information #####")
        for (descr_str, value) in mc.globa.sys_info_telist:
            logging.info(descr_str + ": " + str(value))
        logging.info("#####")

        # Audio setup
        self.sound_effect = None
        try:
            self.sound_effect = QtMultimedia.QSoundEffect(self)
            # -PLEASE NOTE: A parent has to be given here, otherwise we will not hear anything
        except NameError:
            logging.debug("NameError - Cannot play audio since QtMultimedia has not been imported")

        # Timer setup
        self.breathing_timer = Timer()
        self.breathing_timer.timeout_signal.connect(self.on_breathing_timer_timeout)
        self.prepare_timer = Timer()  # -normally 8
        self.prepare_timer.timeout_signal.connect(self.on_prepare_timer_timeout)

        # Window setup
        self.settings_win = None
        self.br_and_rest_dlg = mc.gui.breathing_and_rest_dlg.BreathingAndRestDlg()
        self.br_and_rest_dlg.breathing_dlg.first_breathing_gi_signal.connect(self.on_first_breathing_gi)
        self.br_and_rest_dlg.close_signal.connect(self.on_br_and_rest_closed)

        # Initialization for the user
        self.br_and_rest_dlg.show_breathing_only()
        self.breathing_timer.stop()

        """
        # Startup actions
        if not mc.globa.db_file_exists_at_application_startup_bl and not mc.globa.testing_bool:
            self.show_intro_dialog()
        # self.open_breathing_prepare()

        settings = mc.model.SettingsM.get()
        if settings.nr_times_started_since_last_feedback_notif != mc.globa.FEEDBACK_DIALOG_NOT_SHOWN_AT_STARTUP:
            if (settings.nr_times_started_since_last_feedback_notif
            >= mc.globa.NR_OF_TIMES_UNTIL_FEEDBACK_SHOWN_INT - 1):
                self.show_feedback_dialog()
                settings.nr_times_started_since_last_feedback_notif = 0
            else:
                settings.nr_times_started_since_last_feedback_notif += 1
        else:
            pass
        """

    """
            mc.globa.rest_reminder_minutes_passed_int += 1


    def on_systray_activated(self, i_reason):
        # LXDE:
        # XFCE:
        # MacOS:
        logging.debug("===== on_systray_activated entered =====")
        logging.debug("i_reason = " + str(i_reason))
        logging.debug("mouseButtons() = " + str(QtWidgets.QApplication.mouseButtons()))
        self.tray_icon.activated.emit(i_reason)

        if i_reason == QtWidgets.QSystemTrayIcon.Trigger:
            self.restore_window()
        else:
            self.tray_icon.activated.emit(i_reason)

        logging.debug("===== on_systray_activated exited =====")
        
        

    def update_tooltip(self):
        #Adds tooltip on the systray icon showing the amount of time left until the next break
        settings = mc.model.SettingsM.get()
        if settings.rest_reminder_active:
            self.tray_icon.setToolTip(
                str(
                    mc.model.SettingsM.get().rest_reminder_interval
                    - mc.globa.rest_reminder_minutes_passed_int
                )
                + " " + self.tr("minute/s left until next rest")
            )
        else:
            self.tray_icon.setToolTip(self.tr("Rest breaks disabled"))

    def update_rest_progress_bar(self, time_passed_int: int, interval_minutes_int: int):
        if self.rest_progress_qaction is None:
            return
        time_passed_str = ""
        parts_of_ten_int = (10 * time_passed_int) // interval_minutes_int
        for i in range(0, 9):
            if i < parts_of_ten_int:
                time_passed_str += "◾"
            else:
                time_passed_str += "◽"
        self.rest_progress_qaction.setText(time_passed_str)

        
    
    def on_suspend_application_clicked(self):
        self._suspend_time_dlg = mc.gui.suspend_time_dlg.SuspendTimeDialog()
        self._suspend_time_dlg.finished.connect(self.on_suspend_time_dlg_finished)
        self._suspend_time_dlg.show()

    def on_suspend_time_dlg_finished(self, i_result: int):
        if i_result == QtWidgets.QDialog.Accepted:
            self.start_suspend_timer(self._suspend_time_dlg.suspend_time_qsr.value())

        
    def stop_suspend_timer(self):
        if self.suspend_qtimer is not None and self.suspend_qtimer.isActive():
            self.suspend_qtimer.stop()

    def start_suspend_timer(self, i_minutes: int):
        if i_minutes == 0:
            logging.debug("Resuming application (after suspending)")
            self.stop_suspend_timer()
        logging.debug("Suspending the application for " + str(i_minutes) + " minutes")

        self.stop_rest_timer()
        self.stop_breathing_timer()

        self.suspend_qtimer = QtCore.QTimer(self)  # -please remember to send "self" to the timer
        self.suspend_qtimer.setSingleShot(True)  # <-------
        self.suspend_qtimer.timeout.connect(self.suspend_timer_timeout)
        self.suspend_qtimer.start(i_minutes * 60 * 1000)
        self.update_gui()

    def suspend_timer_timeout(self):
        self.stop_suspend_timer()  # -making sure that this is stopped, just in case

        self.start_rest_timer()
        self.start_breathing_timer()
        self.update_gui()

    """

    def on_tray_quit_triggered(self):
        QtWidgets.QApplication.quit()

    def on_br_and_rest_closed(self):
        br_timer_secs: int = mc.globa.settings.get(mc.globa.SK_BREATHING_BREAK_TIMER_SECS)
        self.breathing_timer.start(br_timer_secs)
        mc.globa.is_breathing_reminder_shown = False
        self.update_systray_image()

    def play_audio(self, i_audio_filename: str, i_volume: int) -> None:
        """
        Please note that the important variable sound_effect is setup at the beginning of the init for settings_win.py
        The reason is that to create this variable we need to send a "self" variable (of which type?)
        :param i_audio_filename:
        :param i_volume:
        :return:
        """
        if self.sound_effect is None:
            logging.warning("play_audio: sound_effect is None")
            return
        audio_path_str = mc.globa.get_user_audio_path(i_audio_filename)
        audio_source_qurl = QtCore.QUrl.fromLocalFile(audio_path_str)
        self.sound_effect.setSource(audio_source_qurl)
        self.sound_effect.setVolume(float(i_volume / 100))
        self.sound_effect.play()

    def on_prepare_timer_timeout(self):
        if self.br_and_rest_dlg.isHidden():
            self.br_and_rest_dlg.show_breathing_only()
            # mc.globa.is_breathing_reminder_shown = False
            self.update_systray_image()

    def on_breathing_timer_timeout(self):
        mc.globa.nr_of_breathing_reminders_since_last_rest += 1
        if mc.globa.nr_of_breathing_reminders_since_last_rest >= 2:
            mc.globa.is_rest_reminder_shown = True
        audio_path_str = mc.globa.get_user_audio_path("small_bell_short[cc0].wav")
        self.play_audio(audio_path_str, 50)
        mc.globa.is_breathing_reminder_shown = True
        self.update_systray_image()
        self.prepare_timer.start(3)

    def on_first_breathing_gi(self):
        audio_path_str = mc.globa.get_user_audio_path("big_bell[cc0].wav")
        self.play_audio(audio_path_str, 50)

    def update_systray_image(self):
        icon_file_name_str = "icon.png"
        if mc.globa.is_breathing_reminder_shown and mc.globa.is_rest_reminder_shown:
            icon_file_name_str = f"icon-br.png"
        elif mc.globa.is_breathing_reminder_shown:
            icon_file_name_str = f"icon-b.png"
        elif mc.globa.is_rest_reminder_shown:
            icon_file_name_str = f"icon-r.png"
        """
        if self.suspend_qtimer is not None and self.suspend_qtimer.isActive():
            icon_file_name_str = "icon-suspend.png"
        """
        systray_icon_path = mc.globa.get_app_icon_path(icon_file_name_str)
        self.tray_icon.setIcon(QtGui.QIcon(systray_icon_path))

    def on_tray_menu_about_to_show(self):
        self.rest_progress_qaction.setText("TBD - time since last rest")

    def on_tray_open_settings_triggered(self):
        self.settings_win = mc.gui.settings_win.SettingsWin()
        self.settings_win.show()

    def on_tray_open_breathing_dialog_triggered(self):
        self.br_and_rest_dlg.show_breathing_only()
        mc.globa.is_breathing_reminder_shown = False
        self.update_systray_image()
        self.breathing_timer.stop()
        self.prepare_timer.stop()

    def on_tray_rest_now_triggered(self):
        self.br_and_rest_dlg.show_full_rest_dialog()
        mc.globa.is_rest_reminder_shown = False
        self.update_systray_image()


class Timer(QtCore.QObject):
    timeout_signal = QtCore.pyqtSignal()

    def __init__(self, i_continuous: bool = False):
        super().__init__()
        # self.minutes_elapsed: int = 0
        self.timeout_secs = -1
        self.timer = None
        self.end_after_next_timeout: bool = not i_continuous

    def stop(self):
        if self.timer is not None and self.timer.isActive():
            self.timer.stop()
        # self.minutes_elapsed = 0

    def start(self, i_timeout_secs: int):
        self.stop()
        self.timeout_secs = i_timeout_secs
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.timeout)
        self.timer.start(self.timeout_secs * 1000)

    def timeout(self):
        # self.minutes_elapsed += 1
        self.timeout_signal.emit()
        print("self.update_signal.emit(True)")
        if self.end_after_next_timeout:
            self.timer.stop()
