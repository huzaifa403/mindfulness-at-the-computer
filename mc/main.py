#!/usr/bin/env python3
import logging
import logging.handlers
import os
import sys
from PyQt5 import QtCore
from PyQt5 import QtWidgets
import mc.globa
import mc.main_object

# The following import looks like it isn't used, but it is necessary for importing the images.
import mc.matc_rc  # pylint: disable=unused-import

LOG_FILE_NAME_STR = "matc.log"


def on_about_to_quit():
    logging.debug("on_about_to_quit --- saving settings to json file (in the user config dir)")
    json_file_path: str = mc.globa.get_settings_file_path()
    mc.globa.save_dict_to_json(mc.globa.settings, json_file_path)


def main():
    # db_filepath: str = mc.globa.get_database_path()
    # mc.globa.db_file_exists_at_application_startup_bl = os.path.isfile(db_filepath)
    # -settings this variable before the file has been created

    logger = logging.getLogger()
    # -if we set a name here for the logger the file handler will no longer work, unknown why
    logger.handlers = []  # -removing the default stream handler first
    # logger.propagate = False
    log_path_str = mc.globa.get_config_path(LOG_FILE_NAME_STR)
    rfile_handler = logging.handlers.RotatingFileHandler(log_path_str, maxBytes=8192, backupCount=2)
    rfile_handler.setLevel(logging.WARNING)
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    rfile_handler.setFormatter(formatter)
    logger.addHandler(rfile_handler)
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s: %(message)s')
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    matc_qapplication = QtWidgets.QApplication(sys.argv)

    # Application information
    mc.globa.sys_info_telist.append(("Application name", mc.globa.APPLICATION_TITLE_STR))
    mc.globa.sys_info_telist.append(("Application version", mc.globa.APPLICATION_VERSION_STR))
    mc.globa.sys_info_telist.append(("Python version", sys.version))
    mc.globa.sys_info_telist.append(("Qt version", QtCore.qVersion()))
    mc.globa.sys_info_telist.append(("PyQt (Python module) version", QtCore.PYQT_VERSION_STR))

    # set stylesheet
    stream = QtCore.QFile(os.path.join(mc.globa.get_base_dir(), "matc.qss"))
    stream.open(QtCore.QIODevice.ReadOnly)
    matc_qapplication.setStyleSheet(QtCore.QTextStream(stream).readAll())

    desktop_widget = matc_qapplication.desktop()
    mc.globa.sys_info_telist.append(("Virtual desktop", str(desktop_widget.isVirtualDesktop())))
    mc.globa.sys_info_telist.append(("Screen count", str(desktop_widget.screenCount())))
    mc.globa.sys_info_telist.append(("Primary screen", str(desktop_widget.primaryScreen())))

    translator = QtCore.QTranslator()
    # Warning While removing debug keep the loading call intact
    system_locale = QtCore.QLocale.system().name()
    logging.info('System Localization: ' + system_locale)
    logging.info(
        'Localization Load Status: ' + str(translator.load(system_locale + '.qm', 'translate/' + system_locale))
    )  # -name, dir
    matc_qapplication.installTranslator(translator)
    matc_qapplication.setQuitOnLastWindowClosed(False)
    matc_qapplication.aboutToQuit.connect(on_about_to_quit)

    matc_main_object = mc.main_object.MainObject()

    """
    matc_main_window = mc.gui.settings_win.SettingsWin()
    if mc.globa.db_upgrade_message_str:
        QtWidgets.QMessageBox.warning(matc_main_window, "title", mc.globa.db_upgrade_message_str)
    """

    sys.exit(matc_qapplication.exec_())


if __name__ == "__main__":
    main()
