import datetime
import shutil
import json
import dataclasses
import enum
import logging
import os
from PyQt5 import QtGui
from PyQt5 import QtCore

#############################################
# This file contains
# * global application state which is not stored in the database (on disk)
# * global functions relating to file names/paths
# * global font functions
# * potentially other global functions
#############################################

APPLICATION_TITLE_STR = "Mindfulness at the Computer"
APPLICATION_VERSION_STR = "1.0.0-alpha.8-dev.2"
APPLICATION_SCRIPT_NAME_STR = "matc"


NO_PHRASE_SELECTED_INT = -1
NO_REST_ACTION_SELECTED_INT = -1
NOTHING_SELECTED_INT = -1
# -TODO: merge these three above into one
FEEDBACK_DIALOG_NOT_SHOWN_AT_STARTUP = -1
NR_OF_TIMES_UNTIL_FEEDBACK_SHOWN_INT = 10

LIST_ITEM_HEIGHT_INT = 30

GRID_VERTICAL_SPACING_LINUX = 15
BUTTON_BAR_HORIZONTAL_SPACING_LINUX = 2

JSON_SETTINGS_FILE_NAME = "settings.json"
APPLICATION_ICON_NAME_STR = "icon.png"
# DATABASE_FILE_STR = "mindfulness-at-the-computer.db"
README_FILE_STR = "README.md"

USER_FILES_DIR_STR = "user_files"
IMAGES_DIR_STR = "images"
ICONS_DIR_STR = "icons"
OPEN_ICONIC_ICONS_DIR_STR = "open_iconic"
AUDIO_DIR_STR = "audio"

BIG_BELL_FILENAME_STR = "big_bell[cc0].wav"
SMALL_BELL_LONG_FILENAME_STR = "small_bell_long[cc0].wav"
WIND_CHIMES_FILENAME_STR = "wind_chimes[cc0].wav"
"""
BIG_BELL_FILE_REF_STR = "qrc:/audio/big_bell[cc0].wav"
SMALL_BELL_LONG_FILE_REF_STR = "qrc:/audio/small_bell_long[cc0].wav"
WIND_CHIMES_FILE_REF_STR = "qrc:/audio/wind_chimes[cc0].wav"
SMALL_BELL_SHORT_FILE_REF_STR = "qrc:/audio/small_bell_short[cc0].wav"
"""
BREATHING_PHRASE_NOT_SHOWN: int = -1
active_rest_action_id_it = NO_REST_ACTION_SELECTED_INT
active_phrase_id_it = 2
rest_window_shown_bool = False
testing_bool = False
rest_reminder_minutes_passed_int = 0
# active_rest_image_full_path_str = "user_files/tea.png"
db_file_exists_at_application_startup_bl = False
display_inline_help_texts_bool = True  # -TODO


class BreathingState(enum.Enum):
    inactive = 0
    breathing_in = 1
    breathing_out = 2


"""
class DialogVisible(enum.Enum):
    breathing_dlg = enum.auto()
    whole_rest_dlg = enum.auto()  # -which includes the breathing dialog
    neither = enum.auto()
    
    Replacing with isvisible and isfullscreen


class ApplicationState:
    def __init__(self):
        self.breathing_reminder_shown: bool = False
        self.rest_reminder_shown: bool = False
        self.nr_of_breathing_reminders_since_last_rest: int = 0
        self.dialog_visible = DialogVisible.neither
"""

is_breathing_reminder_shown: bool = False
is_rest_reminder_shown: bool = False
nr_of_breathing_reminders_since_last_rest: int = 0

MC_LIGHT_GREEN_COLOR_STR = "#bfef7f"
MC_DARK_GREEN_COLOR_STR = "#7fcc19"
MC_DARKER_GREEN_COLOR_STR = "#548811"
MC_WHITE_COLOR_STR = "#ffffff"
MC_BLACK_COLOR_STR = "#1C1C1C"


class PhraseSetup(enum.Enum):
    Long = 0
    Switch = 1
    Short = 2


class NotificationType(enum.Enum):
    Both = 0
    Visual = 1
    Audio = 2


class PhraseSelection(enum.Enum):
    same = 0
    random = 1


breathing_state = BreathingState.inactive


class BreathingVisType(enum.Enum):
    mainwindow_widget = 0
    popup_dialog = 1


def get_config_path(*args) -> str:
    # application_dir_str = os.path.dirname(os.path.dirname(__file__))
    config_dir = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.ConfigLocation)[0]
    config_dir = os.path.join(config_dir, "mindfulness-at-the-computer")
    full_path_str = config_dir
    for arg in args:
        full_path_str = os.path.join(full_path_str, arg)
    os.makedirs(os.path.dirname(full_path_str), exist_ok=True)
    return full_path_str


def get_settings_file_path(i_date_text: str = ""):
    config_path = get_config_path()
    json_file_name: str = JSON_SETTINGS_FILE_NAME
    if i_date_text:
        json_file_name = json_file_name + "_" + i_date_text
    json_file_path = os.path.join(config_path, json_file_name)
    return json_file_path


def get_base_dir() -> str:
    module_dir_str: str = os.path.dirname(os.path.abspath(__file__))
    base_dir_str: str = os.path.dirname(module_dir_str)
    # base_dir_str = os.getcwd()
    # -__file__ is the file that was started, in other words mindfulness-at-the-computer.py
    return base_dir_str


def get_user_images_path(i_file_name: str="") -> str:
    if i_file_name:
        user_images_path_str = os.path.join(get_base_dir(), USER_FILES_DIR_STR, IMAGES_DIR_STR, i_file_name)
    else:
        user_images_path_str = os.path.join(get_base_dir(), USER_FILES_DIR_STR, IMAGES_DIR_STR)
    return user_images_path_str
    # user_dir_path_str = QtCore.QDir.currentPath() + "/user_files/images/"
    # return QtCore.QDir.toNativeSeparators(user_dir_path_str)


def get_user_audio_path(i_file_name: str="") -> str:
    if i_file_name:
        user_audio_path_str = os.path.join(get_base_dir(), USER_FILES_DIR_STR, AUDIO_DIR_STR, i_file_name)
    else:
        user_audio_path_str = os.path.join(get_base_dir(), USER_FILES_DIR_STR, AUDIO_DIR_STR)
    return user_audio_path_str


def get_app_icon_path(i_file_name: str) -> str:
    ret_icon_path_str = os.path.join(get_base_dir(), "res", ICONS_DIR_STR, i_file_name)
    return ret_icon_path_str


def get_icon_path(i_file_name: str) -> str:
    ret_icon_path_str = os.path.join(get_base_dir(), "res", ICONS_DIR_STR, OPEN_ICONIC_ICONS_DIR_STR, i_file_name)
    return ret_icon_path_str


"""
def get_icon_path(i_filename: str) -> str:
    return os.path.join(get_base_dir(), ICONS_DIR_STR, i_filename)

def get_app_icon_path() -> str:
    icon_file_name_str = "icon.png"
    ret_icon_path_str = os.path.join(get_base_dir(), ICONS_DIR_STR, icon_file_name_str)
    return ret_icon_path_str
"""


def get_user_files_path(i_file_name: str) -> str:
    return os.path.join(get_base_dir(), USER_FILES_DIR_STR, i_file_name)


"""
def does_database_exist_started() -> bool:
    if os.path.isfile(DATABASE_FILE_NAME):
        return True
    else:
        return False
"""

# Standard font size is (on almost all systems) 12

# TODO: Rewrite into a single function which takes an enum as argument (small, medium, etc)


def get_font_small(i_italics: bool=False, i_bold: bool=False) -> QtGui.QFont:
    font = QtGui.QFont()
    font.setPointSize(9)
    font.setItalic(i_italics)
    font.setBold(i_bold)
    return font


def get_font_medium(i_italics: bool=False, i_bold: bool=False) -> QtGui.QFont:
    font = QtGui.QFont()
    font.setItalic(i_italics)
    font.setBold(i_bold)
    return font


def get_font_large(i_underscore: bool=False, i_italics: bool=False, i_bold: bool=False) -> QtGui.QFont:
    font = QtGui.QFont()
    font.setPointSize(13)
    font.setUnderline(i_underscore)
    font.setItalic(i_italics)
    font.setBold(i_bold)
    return font


def get_font_xlarge(i_underscore: bool=False, i_italics: bool=False, i_bold: bool=False) -> QtGui.QFont:
    font = QtGui.QFont()
    font.setPointSize(16)
    font.setUnderline(i_underscore)
    font.setItalic(i_italics)
    font.setBold(i_bold)
    return font


def get_font_xxlarge(i_underscore: bool=False, i_italics: bool=False, i_bold: bool=False) -> QtGui.QFont:
    font = QtGui.QFont()
    font.setPointSize(24)
    font.setUnderline(i_underscore)
    font.setItalic(i_italics)
    font.setBold(i_bold)
    return font


def get_html(i_text: str, i_font_size: int=None) -> str:
    ret_str = '<p style="text-align:center;'
    if i_font_size is not None:
        ret_str += 'font-size:' + str(i_font_size) + 'px;'
    ret_str += '">' + i_text + '</p>'
    return ret_str


class EventSource(enum.Enum):
    undefined = 0
    rest_action_changed = 11
    rest_list_selection_changed = 12
    breathing_list_phrase_updated = 21
    breathing_list_selection_changed = 22
    breathing_phrase_deleted = 23
    rest_settings_changed_from_settings = 31
    rest_settings_changed_from_intro = 32
    rest_slider_value_changed = 34
    breathing_settings_changed_from_settings = 3
    breathing_settings_changed_from_intro = 4
    rest_opened = 5
    rest_closed = 6


class MoveDirectionEnum(enum.Enum):
    up = 1
    down = 2


class MinOrMaxEnum(enum.Enum):
    min = "MIN"
    max = "MAX"



db_upgrade_message_str = None


sys_info_telist = []


def clear_widget_and_layout_children(qlayout_or_qwidget) -> None:
    if qlayout_or_qwidget.widget():
        qlayout_or_qwidget.widget().deleteLater()
    elif qlayout_or_qwidget.layout():
        while qlayout_or_qwidget.layout().count():
            child_qlayoutitem = qlayout_or_qwidget.takeAt(0)
            clear_widget_and_layout_children(child_qlayoutitem)  # Recursive call


# ############ Settings #############

JSON_OBJ_TYPE = "__obj_type__"

# Setting Keys
SK_SHOW_BREATHING_TEXT = "show_breathing_text"
SK_REST_ACTIONS = "rest_actions"
SK_BREATHING_AUDIO_VOLUME = "breathing_audio_volume"
SK_BREATHING_BREAK_TIMER_SECS = "breathing_break_timer_secs"
SK_BREATHING_PHRASES = "breathing_phrases"


settings: dict = {
    SK_SHOW_BREATHING_TEXT: True,
    SK_BREATHING_AUDIO_VOLUME: 50,
    SK_REST_ACTIONS: [],
    SK_BREATHING_BREAK_TIMER_SECS: 3,
    SK_BREATHING_PHRASES: [],
}
# -the values given here are the minimum values needed for the application to work
# settings_dict[SETTING_ONE_KEY]


# @dataclasses.dataclass
# In the future we may want to rewrite the three classes below to be dataclasses instead
# https://docs.python.org/3/library/dataclasses.html
# They are supported from Python 3.7

class SettingsListObject:
    def __init__(self, i_id: int):
        self.id: int = i_id


class BreathingPhrase(SettingsListObject):
    def __init__(self, i_id: int, i_in_breath: str, i_out_breath: str):
        super().__init__(i_id)
        self.in_breath: str = i_in_breath
        self.out_breath: str = i_out_breath


class RestAction(SettingsListObject):
    def __init__(self, i_id: int, i_title: str, i_image_path: str):
        super().__init__(i_id)
        self.title: str = i_title
        self.image_path: str = i_image_path


def _get_list_object(settings_key: str, i_list_id: int):
    # -> SettingsListObject
    # SettingsListObject
    list_objects: list = settings[settings_key]
    for o in list_objects:
        if o.id == i_list_id:
            return o
    raise Exception(f"No list object found in the list {settings_key} for the id {i_list_id}")


def get_breathing_phrase(i_id: int) -> BreathingPhrase:
    return _get_list_object(SK_BREATHING_PHRASES, i_id)


def get_rest_action(i_id: int) -> RestAction:
    return _get_list_object(SK_REST_ACTIONS, i_id)


def _add_list_object(i_settings_key: str, i_class, *args):
    list_objects: list = settings[i_settings_key]
    highest_id: int = 0
    if list_objects:
        highest_id: int = max(lo.id for lo in list_objects)
    new_id: int = highest_id + 1
    new_br_phrase = i_class(new_id, *args)
    list_objects.append(new_br_phrase)


def add_breathing_phrase(i_in_breath: str, i_out_breath: str):
    _add_list_object(SK_BREATHING_PHRASES, BreathingPhrase, i_in_breath, i_out_breath)


def add_rest_action(i_title: str, i_image_path: str):
    _add_list_object(SK_REST_ACTIONS, RestAction, i_title, i_image_path)


def _remove_list_object(i_settings_key: str, i_id: int) -> None:
    list_objects: list = settings[i_settings_key]
    for o in list_objects:
        if o.id == i_id:
            list_objects.remove(o)
            return


def remove_breathing_phrase(i_id: int):
    _remove_list_object(SK_BREATHING_PHRASES, i_id)


def remove_rest_action(i_id: int):
    _remove_list_object(SK_REST_ACTIONS, i_id)


def _set_list_object_attributes(i_settings_key: str, i_id: int, **kwargs):
    list_objects: list = settings[i_settings_key]
    for o in list_objects:
        if o.id == i_id:
            for k, v in kwargs.items():
                if getattr(o, k):
                    setattr(o, k, v)
                else:
                    logging.warning(f"Could not find attribute {k} in object {o}. Continuing")
            return


def set_breathing_phrase_attributes(i_id: int, **kwargs):
    # Example: set_breathing_phrase_attributes(1, in_breath="Breathing in, i know i ___")
    # Changing to these arguments? in_breath=i_in_breath, out_breath=i_out_breath
    _set_list_object_attributes(SK_BREATHING_PHRASES, i_id, **kwargs)


def set_rest_action_attributes(i_id: int, **kwargs):
    _set_list_object_attributes(SK_REST_ACTIONS, i_id, **kwargs)



class MyEncoder(json.JSONEncoder):
    def default(self, obj):  # -overridden
        if issubclass(obj, SettingsListObject):
            object_dictionary: dict = obj.__dict__
            type_value = type(obj).__name__
            # if isinstance(obj, BreathingPhrase):
            # type_value = BreathingPhrase.__name__
            # else: raise Exception(f"Cannot endode object: Case is not covered")
            object_dictionary[JSON_OBJ_TYPE] = type_value
            return obj.__dict__
        else:
            return super().default(obj)


def my_decode(dct: dict):
    """
    :param dct:
    From the documentation:
    "object_hook is an optional function that will be called with the result of any object literal
    decoded (a dict). The return value of object_hook will be used instead of the dict."
    :return:
    """
    if JSON_OBJ_TYPE in dct and dct[JSON_OBJ_TYPE] == RestAction.__name__:
        rest_action_obj = RestAction(
            i_id=dct["id"],
            i_title=dct["title"],
            i_image_path=dct["image_path"]
        )
        return rest_action_obj
    return dct


def settings_file_exists() -> bool:
    return os.path.isfile(get_settings_file_path())


def backup_settings_file() -> None:
    if testing_bool:
        return
    date_sg = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    new_file_name_sg = get_settings_file_path(date_sg)
    shutil.copyfile(get_settings_file_path(), new_file_name_sg)

    # Removing older backups

    # CHecking if it's well-formatted (JSON ok?)


def save_dict_to_json(i_dict_to_save: dict, i_file_path: str):
    print("save_data_to_json")
    print(f"{i_dict_to_save=}")
    if settings_file_exists():
        pass
        # backup_settings_file()
    with open(i_file_path, "w") as write_file:
        json.dump(i_dict_to_save, write_file, indent=2, cls=MyEncoder)


def update_dict_with_json_data(i_dict_ref: dict, i_file_path: str):
    print("update_dict_with_json_data")
    if os.path.isfile(i_file_path):
        with open(i_file_path, "r") as read_file:
            from_file_dict: dict = json.load(read_file, object_hook=my_decode)

            diff_key_list: list = []
            for min_key in i_dict_ref.keys():
                if min_key not in from_file_dict.keys():
                    diff_key_list.append(min_key)
            if diff_key_list:
                print(f"One or more keys needed for the application to work were not "
                f"available in {os.path.basename(i_file_path)} so have been added now. "
                f"These are the keys: {diff_key_list}")

            diff_key_list: list = []
            for file_key in from_file_dict.keys():
                if file_key not in i_dict_ref.keys():
                    diff_key_list.append(file_key)
            if diff_key_list:
                print(f"One or more keys in {os.path.basename(i_file_path)} are not "
                f"used by the application (though may have been used before). "
                f"These are the keys: {diff_key_list}")

            print(f"Before merge {i_dict_ref=}")
            print(f"Before merge {from_file_dict=}")
            i_dict_ref.update(from_file_dict)
            # -if there are different values for the same key, the value
            #  in from_file_dict takes precendence
            print(f"After merge {i_dict_ref=}")


# Initial setup
if not settings_file_exists():
    # min_settings_dict[SK_REST_ACTIONS].update(init_rest_actions)
    add_breathing_phrase(i_in_breath="Breathing in _", i_out_breath="Breathing out _")
    add_breathing_phrase(i_in_breath="Breathing in i am aware of my body", i_out_breath="Breathing out i am aware of my body")
    add_breathing_phrase(i_in_breath="May I be happy", i_out_breath="May I be peaceful")
    """
    "Breathing in, I know I am breathing in","Breathing out, I know I am breathing out",
    "Aware of my body, I breathe in","Aware of my body, I breathe out",
    "Breathing in, I care for my body","Breathing out, I relax my body",
    "Happy, At Peace","May I be happy",
    "Breathing in I share the well-being of others","Breathing out I contribute to the well-being of others",
    "Breathing in compassion to myself","Breathing out compassion to others",
    "Self-love and acceptance","I love and accept myself just as I am",
    """

    add_rest_action(i_title="Making a cup of tea", i_image_path="")
    add_rest_action(i_title="Cleaning/organizing my space", i_image_path="")
    add_rest_action(i_title="Walking meditation", i_image_path="")
    """
    "Filling a water bottle for my desk": "/path/to/image",
    "Stretching my arms": "/path/to/image",
    "Opening a window": "/path/to/image",
    "Watering the plants": "/path/to/image",
    "Eating something healthy": "/path/to/image",
    "Walking outside": "/path/to/image",
    """


db_file_exists_at_application_startup_bl = settings_file_exists()
update_dict_with_json_data(settings, get_settings_file_path())
print(f"{settings=}")

"""
def populate_db_with_setup_data() -> None:

def populate_db_with_test_data() -> None:
    populate_db_with_setup_data()

def backup_db_file() -> None:
    if globa.testing_bool:
        return
    date_sg = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    new_file_name_sg = globa.get_database_path(date_sg)
    shutil.copyfile(globa.get_database_path(), new_file_name_sg)

"""