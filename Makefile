# cd in Makefiles: https://stackoverflow.com/q/1789594/2525237

# Running dependencies in order:
# * Option A: target: | dep1 dep2 dep3
# * Option B: make dep1 (indented)

.PHONY: clean
clean:
	rm -rf dist/ build/ Mindfulness_at_the_Computer.egg-info/
	rm -rf dist/ build/
	rm -f mindfulness-at-the-computer.tar.gz

.PHONY: build-pyinstaller
build-pyinstaller:
	make clean
	pyinstaller mindfulness-at-the-computer-linux.spec
	cd dist && tar -czvf mindfulness-at-the-computer.tar.gz mindfulness-at-the-computer/
	mv dist/mindfulness-at-the-computer.tar.gz .

.PHONY: build-pypi
build-pypi:
	make clean
	python3 setup.py sdist bdist_wheel
	python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
	python3 -m webbrowser -t "https://test.pypi.org/project/Mindfulness-at-the-Computer/"
	# python3 -m webbrowser.open\("https://test.pypi.org/project/Mindfulness-at-the-Computer/"\)

.PHONY: run
run:
	cd mc && python3 main.py

.PHONY: test
test:
	python3 -m unittest discover -s test
	# unittest documentation: https://docs.python.org/3/library/unittest.html

