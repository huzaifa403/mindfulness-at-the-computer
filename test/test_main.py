import sys
import unittest
from PyQt5 import QtTest
from PyQt5 import QtCore
from PyQt5 import QtWidgets
import mc.main_object
import mc.globa


test_app = QtWidgets.QApplication(sys.argv)
# -has to be set here (rather than in __main__) to avoid an error


# https://doc.qt.io/qt-5/qpluginloader.html

# https://doc.qt.io/qt-5/qlibraryinfo.html
plugins_path: str = QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.PluginsPath)
print(f"{plugins_path=}")


class MainTest(unittest.TestCase):
    """
    "@unittest.skip" can be used to skip a test
    """

    @classmethod
    def setUpClass(cls):
        mc.globa.testing_bool = True

    def setUp(self):
        pass

    def test_main_object(self):
        i = 0


if __name__ == "__main__":
    unittest.main()

